Terraform template that creates a 3 tier architecture in AWS

Creates a VPC with public and private subnets

Creates an ALB for routing traffic to the frontend web servers

The front end servers are an auto scaling group split across availability zones

An internal ALB is then initialised for routing traffic to the backend web servers

The backend webservers are again another auto scaling group split across the two private subnets.

Finally the third tier is a database server running mysql on Amazon RDS.

With terraform installed the environment can be deployed using

terraform apply

If custom configuration for the deployment is needed this can be done by modifying config.tfvars and deploying with

terraform apply -var-file=config.tfvars