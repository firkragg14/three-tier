variable "aws_region" {
  default = "eu-west-1"
}

variable "deployment_name" {
  default = "test"
}


variable "vpc_range" {
    default = "172.16"  
}

//app servers
variable "app_instance_type" {
    default = "t2.micro"
}

variable "app_autoscale_max" {
    default = 1
}

variable "app_autoscale_min" {
    default = 1
}

//backend servers
variable "backend_instance_type" {
    default = "t2.micro"
}

variable "backend_autoscale_max" {
    default = 1
}

variable "backend_autoscale_min" {
    default = 1
}




//RDS
variable "db_instance_type" {
    type = string
}
variable "db_username" {
    type = string
}
variable "db_password" {
    type = string
}