aws_region = "eu-west-1"
deployment_name = "three_tier_test"
vpc_range = "172.16"

//app
app_instance_type = "t2.micro"
app_autoscale_max = 1
app_autoscale_min = 1

//backend
backend_instance_type = "t2.micro"
backend_autoscale_max = 1
backend_autoscale_min = 1

//RDS
db_instance_type = "db.t3.micro"
db_username = "testuser"
db_password = "testpass"