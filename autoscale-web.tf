resource "aws_launch_configuration" "app-launch-config" {
  image_id        = "ami-096f43ef67d75e998"
  instance_type   = var.app_instance_type
  security_groups = [aws_security_group.inbound_application_group.id, aws_security_group.egress_group.id]

  name_prefix = format("%s-app-", var.deployment_name)

  lifecycle {
    create_before_destroy = true
  }

}

resource "aws_autoscaling_group" "app-asg" {
  launch_configuration = aws_launch_configuration.app-launch-config.id

  vpc_zone_identifier = [aws_subnet.private_subnet_a.id, aws_subnet.private_subnet_b.id]

  health_check_type = "EC2"

  min_size = var.app_autoscale_min
  max_size = var.app_autoscale_max

}

resource "aws_autoscaling_attachment" "app_asg_attachment" {
  autoscaling_group_name = aws_autoscaling_group.app-asg.id
  alb_target_group_arn   = aws_lb_target_group.app-target-group.arn
}