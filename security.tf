resource "aws_security_group" "egress_group" {
  description = "HTTP routing to the internet"
  vpc_id      = aws_vpc.three_tier_vpc.id

  tags = {
    Project = "three-tier-architecture"
  }
}

resource "aws_security_group" "ssh_access_group" {
  description = "SSH inbound to the public subnet"
  vpc_id      = aws_vpc.three_tier_vpc.id

  tags = {
    Project = "three-tier-architecture"
  }
}

resource "aws_security_group" "inbound_application_group" {
  description = "SSH ingress and web traffic from load balancers"
  vpc_id      = aws_vpc.three_tier_vpc.id
  tags = {
    Project = "three-tier-architecture"
  }
}