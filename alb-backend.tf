resource "aws_alb" "alb_backend_servers" {
  name               = "alb-backend"
  internal           = true
  load_balancer_type = "application"

  subnets            = [aws_subnet.private_subnet_a.id, aws_subnet.private_subnet_b.id]
  security_groups    = [aws_security_group.inbound_application_group.id]

  tags = {
    Project = "three-tier-architecture"
  }
}

resource "aws_alb_listener" "alb_backend_listener" {  
  load_balancer_arn = aws_alb.alb_web_servers.arn
  port              = 80  
  protocol          = "HTTP"
  
  default_action {    
    target_group_arn = aws_lb_target_group.app-target-group.arn
    type             = "forward"  
  }
}


resource "aws_lb_target_group" "backend-target-group" {
  name        = "backend-target-group"
  port        = 80
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = aws_vpc.three_tier_vpc.id
}
