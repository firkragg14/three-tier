resource "aws_vpc" "three_tier_vpc" {
  cidr_block = "172.16.0.0/16"

  tags = {
    Project = "three-tier-architecture"
  }
}

resource "aws_subnet" "public_subnet_a" {
  vpc_id                  = aws_vpc.three_tier_vpc.id
  cidr_block              = format("%s.1.0/24", var.vpc_range)
  availability_zone       = format("%sa", var.aws_region)
  map_public_ip_on_launch = true

  tags = {
    Project = "three-tier-architecture"
  }
}

resource "aws_subnet" "public_subnet_b" {
  vpc_id                  = aws_vpc.three_tier_vpc.id
  cidr_block              = format("%s.2.0/24", var.vpc_range)
  availability_zone       = format("%sb", var.aws_region)
  map_public_ip_on_launch = true

  tags = {
    Project = "three-tier-architecture"
  }
}

resource "aws_subnet" "private_subnet_a" {
  vpc_id                  = aws_vpc.three_tier_vpc.id
  cidr_block              = format("%s.4.0/24", var.vpc_range)
  availability_zone       = format("%sa", var.aws_region)
  map_public_ip_on_launch = false

  tags = {
    Project = "three-tier-architecture"
  }
}

resource "aws_subnet" "private_subnet_b" {
  vpc_id                  = aws_vpc.three_tier_vpc.id
  cidr_block              = format("%s.5.0/24", var.vpc_range)
  availability_zone       = format("%sb", var.aws_region)
  map_public_ip_on_launch = false

  tags = {
    Project = "three-tier-architecture"
  }
}

resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = aws_vpc.three_tier_vpc.id

  tags = {
    Project = "three-tier-architecture"
  }
}